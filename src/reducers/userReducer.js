import { List, fromJS } from 'immutable'

const initialUser = {
  name: 'Xu Yue',
  age: 26
}

export default (state = fromJS(initialUser), action) => {
  switch (action.type) {
    case 'CHANGE_NAME':
      return state.set('name', action.payload)
    case 'CHANGE_AGE':
      return state.set('age', action.payload)
  }
  return state
}
