import { List, fromJS } from 'immutable'

const initialPosts = {
  pending: false,
  items: []
}

export default (state = fromJS(initialPosts), action) => {
  switch (action.type) {
    case 'CREATE_POST_FULFILLED':
      return state.set('items', state.get('items').unshift(action.payload))
    case 'FETCH_POSTS_PENDING':
      return state.set('pending', true)
    case 'FETCH_POSTS_FULFILLED':
      return state.merge(state, { items: List(action.payload), pending: false })
  }
  return state
}
