import { combineEpics, createEpicMiddleware } from 'redux-observable'
import user from './userEpics'
import posts from './postsEpics'
import { Observable } from 'rxjs'
import 'rxjs/add/operator/delay'
import 'rxjs/add/operator/mapTo'
import 'rxjs/add/operator/switchMap'
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/takeUntil'

const epics = combineEpics(user, posts)

export default createEpicMiddleware(epics, {
  dependencies: { fetch$: Observable.fromPromise }
})
