import { Observable } from 'rxjs'

export default action$ =>
  action$
    .ofType('CHANGE_NAME')
    .delay(1000)
    .switchMap(() => Observable.interval(1000))
    .map(n => {
      return {
        type: 'CHANGE_AGE',
        payload: n
      }
    })
