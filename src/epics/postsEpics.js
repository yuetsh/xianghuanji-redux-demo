

const createdPost = post =>
  fetch('https://jsonplaceholder.typicode.com/posts', {
    method: 'POST',
    headers: { 'content-type': 'application/json' },
    body: JSON.stringify(post)
  }).then(res => res.json())

export default (action$, store, { fetch$ }) =>
  action$
    .ofType('CREATE_POST')
    .switchMap(action => fetch$(createdPost(action.payload)))
    .map(post => {
      return {
        type: 'CREATE_POST_FULFILLED',
        payload: post
      }
    })
