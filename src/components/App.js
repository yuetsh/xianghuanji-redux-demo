import React from 'react'
import { connect } from 'react-redux'
import { setName } from '../actions/userActions'
import { fetchPosts } from '../actions/postsActions'
import Form from './Form'

const PostItem = post => (
  <div key={post.id}>
    <h2>{post.id}-{post.title}</h2>
    <p>{post.body}</p>
  </div>
)

@connect(
  store => ({
    user: store.user.toJS(),
    posts: store.posts.toJS()
  }),
  dispatch => ({
    fetchPosts: () => dispatch(fetchPosts()),
    setName: name => dispatch(setName(name))
  })
)
export default class App extends React.PureComponent {
  componentDidMount () {
    const { fetchPosts, setName } = this.props
    fetchPosts()
    setName('Hi')
  }
  render () {
    const { user, posts } = this.props
    return (
      <div>
        <h1>{user.name} - {user.age}</h1>
        <Form />
        {posts.pending ? <div>loading</div> : posts.items.map(PostItem)}
      </div>
    )
  }
}
