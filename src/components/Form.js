import React from 'react'
import { connect } from 'react-redux'
import styled from 'styled-components'
import { createPost } from '../actions/postsActions'

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  width: 300px;
`
@connect(
  store => ({
    posts: store.posts.toJS()
  }),
  dispatch => ({
    createPost: post => dispatch(createPost(post))
  })
)
export default class Form extends React.PureComponent {
  state = {
    title: '',
    body: ''
  }
  onchange = e => {
    this.setState({
      [e.target.name]: e.target.value
    })
  }

  sumbit = async () => {
    await this.props.createPost(this.state)
    this.setState({
      title: '',
      body: ''
    })
  }

  render () {
    const { title, body } = this.state
    const { posts } = this.props
    return (
      <Wrapper>
        <input
          type='text'
          name='title'
          onChange={this.onchange}
          value={title}
        />
        <textarea
          name='body'
          cols='30'
          rows='10'
          onChange={this.onchange}
          value={body}
        />
        <button onClick={this.sumbit}>sumbit</button>
      </Wrapper>
    )
  }
}
