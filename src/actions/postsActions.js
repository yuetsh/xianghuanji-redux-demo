// promise
export const fetchPosts = () => {
  return {
    type: 'FETCH_POSTS',
    payload: fetch('https://jsonplaceholder.typicode.com/posts').then(res =>
      res.json()
    )
  }
}

// observable
export const createPost = post => {
  return {
    type: 'CREATE_POST',
    payload: post
  }
}
